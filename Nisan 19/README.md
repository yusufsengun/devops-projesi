<h1>  Trendyol System Bootcamp Mezuniyet Projesi  </h1>
<h1> 1.Case</h1>
<h2>1.1 Centos Kurulumu ve Güncellemesi</h2>
<br>
<b>->  İlk olarak virtual box üzerine bir centos7 kurdum. </b><br><br>
![virtualbox](/uploads/3dde7e8c9490cd79f9ddfbd7bc43ac84/virtualbox.PNG) <br>
<b>->  "sudo yum update" komutu ile centos7 makineyi güncelledim </b><br>

<h2>1.2 Centos kullanıcı yaratılamsı</h2>
<h4> Kullanıcı yaratmak ve wheel grubuna ekelemek için aşağıdaki komtuları sırasıyla çalıştırdım.<h4>
<b>-> useradd yusuf.sengun </b> <br>
<b>-> usermod -a -G wheel yusuf.sengun</b><br>

<h2>1.3 Sisteme hardisk eklenmesi ve mount edilmesi</h2>
<b>-> Virtual Box ile manuel olarak 10GB trendyol adında bir hardisk ekledim. </b><br>
![Ekran_Alıntısı](/uploads/ef66f42c018abe6bf710bdab5933eba1/Ekran_Alıntısı.PNG)<br>
<b>-> "mount  /dev/sdb1 /mnt/bootcamp " komutu koşarak formatlamış olduğum diski /mnt/bootcamp olarak mount ettim</b>  <br>
![Ekran_Alıntısı](/uploads/787c9ae5d7dbd3a8d14f4821cbe27d0a/Ekran_Alıntısı.PNG)<br>

<h2>1.4 bootcamp.txt dosyası oluşturulması ve içerik yazılması </h2>
<b>/opt/bootcamp/ altında bootcamp.txt dosyası oluşturup dosya içine gerekli yazıyı yazdım</b>
![Ekran_Alıntısı](/uploads/232d6494dbf142ecdd4f600cefddede0/Ekran_Alıntısı.PNG)<br>

<h2>1.5 bootcamp.txt dosyası oluşturulması ve içerik yazılması </h2>
<b>-> /opt/bootcamp/  oluşturduğum dosyayı /home/yusuf.sengun dizinin altına  "sudo find / -name 'bootcamp.txt' -exec cp {} /home/yusuf.sengun/ \;" komutu ile kopyaladım </b><br>
![Ekran_Alıntısı](/uploads/e2e0bd8f716078918c1b16f11cc65b7a/Ekran_Alıntısı.PNG)<br>


<h1>2.Case</h1>

<label>İlk olarak virtualbox'da iki adet centos 7 kurulumu yaptım bunlardan birtanesi ansible control için kullancağım diğeri ise kontrol edilen makine olucak.Kontrol makinemden ansible ile kontrol edilecek makineye ping atarak bağlantıyı kontrol ettim.</label><br>
![Ekran_Alıntısı](/uploads/99f4b0fa62a7daae7118dd3cb10c57ec/Ekran_Alıntısı.PNG)<br>
<h2>2.1 Ansible ile Docker Kurulumu Ayağa Kaldırılması ve bir uygulama dockerized edilmesi</h2>
<label><b> KOmtrol edeceğim makineye Docker yükelmek için install_docker.yml adında bir dosya oluşturdum daha sonra aşağıdaki satırları sırasıyla çalıştırdım. (oluşturduğum .yaml dosyasını repo'da bulabilirsiniz.)</b></label><br>
<label>-> ansible-playbook install_docker.yml --syntax-check</label><br>
<label>-> ansible-playbook install_docker.yml -u root --ask-pass </label>
<label> böylece kontrol edilecek makinede docker kurulmuş oldu.</label>
![Ekran_Alıntısı](/uploads/3a24800dc1a4f112ff75f7acb191b79e/Ekran_Alıntısı.PNG)
